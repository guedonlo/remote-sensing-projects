#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun May 10 17:43:35 2020

@author: sophie
"""

import matplotlib.pyplot as plt
import numpy as np
import netCDF4
import xlrd

#on cherche à extraire l'irradiance (ssi) pour grenoble : 
#45°E 5°N (x=5° y=45°)
# 1 intervalle <-> 0,005° (voir figure)
# donc le pixel correspondant est i=1300 et j=2100

Isat = []
for i in range (261,366):
    filename ='eumetsat ({}).nc'.format(i)
    nc = netCDF4.Dataset(filename, "r")
    ssi = nc.variables['ssi']
    dli = nc.variables['dli']
    ira = ssi[1300][2100]
    Isat.append(ira)

for i in range (1,261):
    filename ='eumetsat ({}).nc'.format(i)
    nc = netCDF4.Dataset(filename, "r")
    ssi = nc.variables['ssi']
    dli = nc.variables['dli']
    ira = ssi[1300][2100]
    Isat.append(ira)
  
#print(ssi)
#print(' ')
#print(dli)
#print(' ')

#ouverture des donnés du capteurs pour la comparaison
Icapteur = []
document = xlrd.open_workbook("donnees_annee.xlsx")
feuille_1 = document.sheet_by_index(0)

for k in range (365):
    cellule = feuille_1.cell_value(4 + k ,24)
    Icapteur.append(cellule)
    


# comparaison  
T = np.array([i for i in range(1,366)])
plt.figure()
plt.plot(T,Icapteur)
plt.plot(T,Isat)

#on trace la différence entre les deux courbes
Id = []
for k in range(len(Isat)):
    Id.append(Isat[k] - Icapteur[k])

plt.figure()
plt.plot(T,Id)

plt.show()


