# -*- coding: utf-8 -*-
"""
Created on Thu May 27 09:28:31 2021

@author: banzeta
"""


def pixelsnow(imageNDSI, bandR, N1, R1):
    sizex=len(imageNDSI)
    sizey=len(imageNDSI[0])
    list_pix_snow = []
    for i in range(sizex): # We sweep the horizontal
        for j in range (sizey): # we then sweep the vertical: we proceed by columns
            if imageNDSI[i][j]>N1 and bandR[i][j]>R1:
                list_pix_snow.append([i,j])
    return list_pix_snow
