# -*- coding: utf-8 -*-
"""
Created on Thu May 27 09:54:41 2021

@author: banzeta
"""
def filename() :
    print("\nHow do you want to called the file with data ?") # Choice of the name of the result file
    name = input()
    if name+".txt" in os.listdir() :
        while name+".txt" in os.listdir(): # Loop for the choice of the name
            print ("\nThis file already exists, do you want to delete it\n\nYes : 1  No : 0")
            m = int(input())
            if m == 1:
                os.remove(str(name)+".txt")
            if m == 0:
                print("\nHow do you want to called the file with data ?")
                name = input()
    return name
