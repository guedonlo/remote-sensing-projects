import os
from PIL import Image
import numpy as np 
import imageio as io


#### declaration of fonctions


def Calculation_NDSI(bandG, bandMIR):
    sizex=len(bandG)
    sizey=len(bandMIR)
    imageNDSI=np.zeros((sizex, sizey))
    imageNDSI=(bandG-bandMIR)/(bandG+bandMIR)
    return imageNDSI

def pixelsnow(imageNDSI, bandR, N1, R1):
    sizex=len(imageNDSI)
    sizey=len(imageNDSI[0])
    list_pix_snow = []
    for i in range(sizex): # We sweep the horizontal
        for j in range (sizey): # we then sweep the vertical: we proceed by columns
            if imageNDSI[i][j]>N1 and bandR[i][j]>R1:
                list_pix_snow.append([i,j])
    return list_pix_snow

def percentagesnow(list_pix_snow,imageNDSI):
    sizex=len(imageNDSI)
    sizey=len(imageNDSI[0])
    pcsnow=[]
    nb_pix_snow=len(list_pix_snow)
    pcsnow.append(int(100*nb_pix_snow/(sizex*sizey)))
    return pcsnow

def filename() :
    print("\nHow do you want to called the file with data ?") # Choice of the name of the result file
    name = input()
    if name+".txt" in os.listdir() :
        while name+".txt" in os.listdir(): # Loop for the choice of the name
            print ("\nThis file already exists, do you want to delete it\n\nYes : 1  No : 0")
            m = int(input())
            if m == 1:
                os.remove(str(name)+".txt")
            if m == 0:
                print("\nHow do you want to called the file with data ?")
                name = input()
    return name


###SCRIPT
    
#import of images

imgpilG=Image.open("SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B3.tif")
bandG=np.array(imgpilG)

imgpilR=Image.open("SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B4.tif")
bandR=np.array(imgpilR)

imgpilMIR=Image.open("SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B8.tif")
bandMIR=np.array(imgpilMIR)



#NDSI treatment

imageNDSI=Calculation_NDSI(bandG, bandMIR)

N1=0.4
R1=0.2

list_pix_snow = pixelsnow(imageNDSI, bandR, N1, R1)


#calculation of snow percentage

pcsnow=percentagesnow(list_pix_snow,imageNDSI)

#results

print("\nInput the value of the scale (distance in meter of one pixel")
scale = int(input()) # the user enters the size of the side of a pixel, for all images

name=filename()

File = open(str(name)+".txt","a") # open the text file that you have previously named to complete it
File.write("\n") # skip a line
File.write("date of the image = ")
File.write("2016,09,22")
File.write("\nNumber of pixels of snow = ")
File.write(str(len(list_pix_snow))) # we have to write a string : we transform
File.write("\nTotal area of  snow = ")
File.write(str(len(list_pix_snow)*(scale**2)))
File.write(" m²")
File.write("\npercentage of snow = ")
File.write(str(pcsnow))
File.write(" %")
File.write("\n____________") # delimitations between calculations




######## pleiades images ########

# image import 

filename2 = "Zone1_fused.tif"
img2 = io.imread(filename2)
print(img2.shape)
print(img2.dtype)
print((img2.min(), img2.max()))



BandG1,BandR1,BandMIR1=np.array(img2[:,1]),np.array(img2[:,2]),np.array(img2[:,3])

# NDSI treatment 

imageNDSI1=Calculation_NDSI(BandG1, BandMIR1)
list_pix_snow1 = pixelsnow(imageNDSI1, BandR1, N1, R1)

#calculation of snow percentage

pcsnow1=percentagesnow(list_pix_snow1,imageNDSI1)


#results

print("\nInput the value of the scale (longueur en mètre d'un pixel)")
File.write("\n") # skip a line
File.write("\n") # skip a line
File.write("\n") # skip a line
File.write("\n") # skip a line
File.write("\n") # skip a line
File.write(str(len(list_pix_snow1))) # we have to write a string : we transform
File.write("\nTotal area of  snow for Pleiade Image = ")
File.write(str(len(list_pix_snow1)*(scale**2)))
File.write(" m²")
File.write("\npercentage of snow for Pleiade Image = ")
File.write(str(pcsnow1))
File.write(" %")
File.write("\n____________") # delimitations between calculations
File.close()
