# -*- coding: utf-8 -*-
"""
Created on Thu May 27 09:10:51 2021

@author: banzeta
"""

import numpy as np 


def Calculation_NDSI(bandG, bandMIR):
    sizex=len(bandG)
    sizey=len(bandMIR)
    imageNDSI=np.zeros((sizex, sizey))
    imageNDSI=(bandG-bandMIR)/(bandG+bandMIR)
    return imageNDSI
