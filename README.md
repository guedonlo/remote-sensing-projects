![./docs/figs/Ense3.png](./docs/figs/Ense3.png)
# Remote Sensing Projects
Remote Sensing Projects at ENSE3, Grenoble-INP.

## Announcements
Announcements will be posted here.

  * **Welcome to the remote sensing project**

  Contacts

    * Mauro Dalla Mura <mauro.dalla-mura@gipsa-lab.grenoble-inp.fr>
    * Aneline Dolet <aneline.dolet@gipsa-lab.grenoble-inp.fr>

  * Important dates:
    * 17/05 Intermediate presentation - 5 min pitch (3 slides)
    * 07/06 Final presentation (15 min presentation + 5-10 min Q&A)


## Course information
### Introduction
Remote sensing refers to the analysis of images and signals acquired
over a physical phenomenon in order to obtain information on the surface
of the Earth (including the atmosphere and the oceans), without direct
contact with it.
Thanks to the specificities of the multiple imaging sensors (sensor with very high spatial resolution or very high spectral resolution, radar, Lidar, etc.) and the multiplicity of their supports (satellites in particular), remote sensing is now widely used for problems of observation and monitoring of the environment.

In this project, students will put into practice the knowledge acquired
in image and signal processing courses on 6 different remote sensing applications related to the observation of the environment.

This project will be developed in python.

### Learning objectives
The main learning objectives of this project are:

* Get familiar with satellite remote sensing and see more in depth an
    applications related to Earth observation
* Apply concepts of image/signal processing and data science (machine/deep learning) for the analysis of remote sensing images

In addition, working on this project have secondary objectives:

* Experience working on project development in a collaborative environment
* Gain experience in python programming (python and jupyter)
* Get familiar with version control (git)
* Get experience with other tools such as markdown, latex, running
  scripts in the terminal...
* Work in English (at least for code development and reporting)
* Contribute to an open project

## Organization
This project is organized into 10 session of 4 hours each.

Some of the sessions will be done remotely some in person at ENSE3.
We will use **discord** for interactions during the remote sessions.
### Useful resources
- Python and Jupyter
  - <https://jakevdp.github.io/PythonDataScienceHandbook/index.html>
    - Ch 1, 2, 4 (Ipython, NumPy, Matplotlib)
    - [optional] Ch. 3, 5 (Pandas, Machine Learning)
    - [jupyter.pdf](./helpers/python/jupyter.pdf)
  - [optional] Tutorial on Jupyter showcasing jupyter features and installation  <https://www.youtube.com/watch?v=HW29067qVWk>
  - [optional] Video on downsides of using jupyter notebooks
  <https://www.youtube.com/watch?v=7jiPeIFXb6U>
  - conda cheatsheet <https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf>

- Git
  - Introductory videos at <https://git-scm.com/videos> [~30 min in
    total]
## Projects
### [1 - Snow cover estimation based on remote sensing high spatial resolution images](1-snow_hr)

![Sentinel-2](./docs/figs/S2_snow.png)

### [2 - Snow cover estimation based on remote sensing time series](2-snow_ts)

![MODIS_feb](./docs/figs/MODIS_feb.png)
![MODIS_may](./docs/figs/MODIS_may.png)

### [3 - Mapping the Grenoble urban environment by remote sensing](3-urban)

![urban](./docs/figs/Grenoble_classif.png)

### [4 - Monitoring pollution in the atmosphere](4-pollution)

![NO2map](./docs/figs/NO2map.png)

### [5 - Analysis of the forest around Grenoble using remote sensing imaging](5-forest)

![Forest](./docs/figs/Cluster.png)

### [6 - Solar irradiance estimation at GreEn-Er](6-irradiance)

![irradiance](./docs/figs/cam2_UTC_19-01-02_10-59-59-84.jpg)

### [7 - Urban Monitoring: Change Detection Bundle](7-change_detection)

![change_detection](./docs/figs/change-detection.jpg)

### [8 - Ship detection challenge](8-ship_detection)

![Example of image acquired by SPOT for ship
detection.](../docs/figs/ships_xs.jpg)

### [9 - Remote Sensing video analysis](9-rs_video)

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://sandbox.intelligence-airbusds.com/web/assets/mp4/beach.mp4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Supervisors
    - Mauro Dalla Mura (2016-2021)
    - Aneline Dolet (2019-2021)
    - Théo Masson (2017-2018)
    - Guillaume Tochon (2016)
